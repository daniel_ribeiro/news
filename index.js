var url = 'https://newsapi.org/v2/top-headlines?' +
          'country=us&' +
          'pageSize=10&' +
          'apiKey=982a9329256040a4ae8a107e4202a253';

var req = new Request(url);
fetch(req)
.then((response) => response.json())
.then((data) => {
    var actv = "active"
    var i = 0
    var foot = ''

    data.articles.forEach(n => {
        if(i > 0) actv = '';
        
        n.publishedAt = n.publishedAt.replace('T',' ').replace('Z',' ')
        
        foot = ''
        if(n.author !== null) foot = n.author + ' - '
        foot += n.publishedAt

        $('.carousel-inner').append(
            `<div class="carousel-item ${actv}" style="background-image: url('${n.urlToImage}')">
                <div class="carousel-caption d-none d-md-block">
                <h3>${n.description}</h3>
                <p>${foot}</p>
                </div>
            </div>`
        )
        $('.carousel-indicators').append(
            `<li data-target="#carouselExampleIndicators" data-slide-to="${i}" class="${actv}"></li>`
        )
        i++
    })
})